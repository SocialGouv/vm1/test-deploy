FROM node:12.14-alpine

WORKDIR /app

ADD package.json .
RUN yarn

ADD src ./src

ENTRYPOINT ["yarn", "start"]