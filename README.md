# docker-compose example

Sample docker-compose project with GitLab CI.

Use docker-compose on a target VM to deploy environments.

Components :

- API NodeJS
- Postgres

## Pipeline

- feature-branch deployments
- master deployment
