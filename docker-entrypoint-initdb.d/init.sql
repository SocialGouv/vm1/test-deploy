CREATE USER example WITH PASSWORD 'password';
CREATE DATABASE example;

\c example;
CREATE TABLE quotes ( quote varchar(200) NOT NULL );
INSERT INTO quotes values ('hello, world 42');

GRANT ALL PRIVILEGES ON DATABASE example TO example;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO example;


