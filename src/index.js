const pgp = require("pg-promise")();
const express = require("express");

const db = pgp(
  process.env.DATABASE_URL || "postgres://username:password@host:port/database"
);

const app = express();

const sha = process.env.CI_COMMIT_SHA;
const ref = process.env.CI_COMMIT_REF_SLUG;
const host = process.env.CI_HOST_NAME;

app.get("/", (req, res) => {
  db.one("SELECT quote from quotes")
    .then(({ quote }) => {
      res.send({
        success: true,
        value: quote,
        sha,
        ref,
        host,
        test: "runner 4"
      });
    })
    .catch(error => {
      console.error("error", error);
      res.send({ success: false, error: error.message });
    });
});

const port = process.env.PORT || 3333;

app.listen(port, () =>
  console.log(`Example app listening on port http://127.0.0.1:${port}!`)
);
